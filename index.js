/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';

import {name as appName} from './app.json';
import Gaurav from './Gaurav';

AppRegistry.registerComponent(appName, () => Gaurav);
