import { StyleSheet, Text, View, Dimensions, Platform, TouchableOpacity, } from 'react-native'

import React, { useState } from 'react'
import { DateTimePicker } from '@react-native-community/datetimepicker';
const { height, width } = Dimensions.get("window");

export default function App() {
  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState('date')
  const [show, setShow] = useState(false)
  const [text, setText] = useState('Empty')

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === "ios");
    setDate(currentDate);


    let tempDate = new Date(currentDate);
    let fDate = tempDate.getDate() + '/' + (tempDate.getMonth() + 1) + '/' + tempDate.getFullYear();
    let fTime = 'Hours: ' + tempDate.getHours() + ' | Minutes' + tempDate.getMinutes();
    setText(fDate + '\n' + fTime)

    console.log(fDate + '(' + fTime + ')')

  }
  const showMode = (currengtMode) => {
    setShow(true);
    setMode(currengtMode);
  }
  return (
    <View style={styles.main}>
      <Text style={{ fontWeight: 'bold', fontSize: 20 }}>{text}</Text>

      <TouchableOpacity onPress={() => showMode('date')}>
        <View style={{ padding: 20 }} >
          <Text> Show</Text>
        </View>
      </TouchableOpacity>

      {show && (
        <DateTimePicker
          testID='dateTimePicker'
          value={date}
          mode={mode}
          is24Hour={true}
          display='default'
          onChange={onChange}
        />
      )
      }
    
    </View>
  );
}



const styles = StyleSheet.create({
  main: {
    height: height / 1,
    width: width / 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})