import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React, { useState } from 'react';
import DateTimePicker from '@react-native-community/datetimepicker';

const Gaurav = () => {
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date')
    const [show, setShow] = useState(false)
    const [text, setText] = useState('Empty')






    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === "ios");
        setDate(currentDate);


        let tempDate = new Date(currentDate);
        let fDate = tempDate.getDate() + '/' + (tempDate.getMonth() + 1) + '/' + tempDate.getFullYear();
        let fTime = 'Hours: ' + tempDate.getHours() + ' | Minutes' + tempDate.getMinutes();
        setText(fDate + "\n" + fTime)

        console.log(fDate + '(' + fTime + ')')

    }


    const showMode = (currengtMode) => {
        setShow(true);
        setMode(currengtMode);
    }







    return (
        <View style={styles.main}>
            <Text>{text}</Text>
            <View style={{ padding: 50 }}>
                <TouchableOpacity onPress={() => showMode('date')}>
                    <Text>Date</Text>
                    </TouchableOpacity>

            </View>
            <View style={{ padding: 50 }}>
             
                <TouchableOpacity onPress={() => showMode('time')}>
                    <Text>Time</Text></TouchableOpacity>

            </View>
            <View style={{ padding: 50 }}>
               
            </View>

{show && (<DateTimePicker
testID='dateTimePicker'
value={date}
mode={mode}
is24Hour={true}
display="default"
onChange={onChange}
/>)}



        </View>
    )
}

export default Gaurav

const styles = StyleSheet.create({
    main: {
        flex: 1,
        backgroundColor: 'cyan',
        justifyContent: 'center',
        alignItems: 'center'
    }
})